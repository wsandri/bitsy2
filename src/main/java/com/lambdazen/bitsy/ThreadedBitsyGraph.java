package com.lambdazen.bitsy;

import java.util.Set;

import com.lambdazen.bitsy.tx.BitsyTransaction;
import com.lambdazen.bitsy.tx.BitsyTransactionContext;
import com.tinkerpop.blueprints.Element;
import com.tinkerpop.blueprints.Features;
import com.tinkerpop.blueprints.TransactionalGraph;

public class ThreadedBitsyGraph extends BitsyGraph implements TransactionalGraph {
    BitsyGraph underlyingGraph;
    BitsyTransaction tx;
    
    public ThreadedBitsyGraph(BitsyGraph g) {
        // Using protected constructor that doesn't create a graph store
        super('_', g.isFullGraphScanAllowed());
        
        this.underlyingGraph = g;
        this.tx = null;
    }
    
    public String toString() {
        return underlyingGraph.toString();
    }
    
    @Override
    public Features getFeatures() {
        return underlyingGraph.getFeatures();
    }

    @Override
    public <T extends Element> void createKeyIndex(String key, Class<T> elementType) {
        underlyingGraph.createKeyIndex(key, elementType);
    }

    @Override
    public <T extends Element> void dropKeyIndex(String key, Class<T> elementType) {
        underlyingGraph.dropKeyIndex(key, elementType);
    }

    @Override
    public <T extends Element> Set<String> getIndexedKeys(Class<T> elementType) {
        return underlyingGraph.getIndexedKeys(elementType);
    }
    
    @Override
    protected ITransaction getTx() {
        // Overriding the getTx() method ensures that the work will be done on
        // the local transaction, NOT the ThreadLocal transaction 
        if (tx == null) {
            this.tx = new BitsyTransaction(new BitsyTransactionContext(underlyingGraph.getStore()), getDefaultIsolationLevel());
        }

        return tx; 
    }

    @Override
    /** This method can be used to check if the current threaded-graph is actively executing a transaction */
    public boolean isTransactionActive() {
        return (tx != null);
    }

    public BitsyIsolationLevel getDefaultIsolationLevel() {
        return underlyingGraph.getDefaultIsolationLevel();
    }
    
    public void setDefaultIsolationLevel(BitsyIsolationLevel level) {
        underlyingGraph.setDefaultIsolationLevel(level);
    }
    
    public BitsyIsolationLevel getTxIsolationLevel() {
        return getTx().getIsolationLevel();
    }
    
    public void setTxIsolationLevel(BitsyIsolationLevel level) {
        getTx().setIsolationLevel(level);   
    }
    
    @Deprecated
    public void stopTransaction(Conclusion conclusion) {
        stopTx(conclusion == Conclusion.SUCCESS);
    }
    
    @Override
    public void commit() {
        stopTx(true);
    }

    @Override
    public void rollback() {
        stopTx(false);
    }
    
    public void stopTx(boolean commit) {
        if (tx == null) {
            // Nothing to do
        } else {
            try {
                // Stop the old transaction if it exists
                tx.save(commit);
            } finally {
                // Remove this transaction -- independent of success/failure
                this.tx = null;
            }
        }
    }

    @Override
    public TransactionalGraph startTransaction() {
        throw new UnsupportedOperationException("Can not startTransaction on a threaded transaction graph");
    }
    
    @Override
    public void shutdown() {
        // As per Blueprints tests, shutdown() implies automatic commit
        stopTx(true);
        
        // Don't mess with the graph store -- this is only a ThreadedGraph, not the main one
    }

}
