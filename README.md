[The project Wiki](https://bitbucket.org/lambdazen/bitsy/wiki/Home) is the official source of documentation. 


### Update on license

Bitsy from AGPL to an Apache 2.0 license as of Aug 8, 2016 in release 1.5.2. 

### Git branching strategy

Tags are named release-[version]. For e.g., release-1.2

A note on branch names:

- master: Unstable snapshot of the software on the last-released major version
- dev-[major-version]-branch: Contains newer major versions that are not ready for release. For e.g., dev-1.5-branch
- release-[major-version].x-branch: Contains older major versions which are supported. For e.g., release-1.x-branch

